package com.example.a2lesson8;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private Bitmap mCameraBitmap;
	private ListView imagelistview;	
	private Button saveall;
	private static ArrayList<Bitmap>  imagelist;
	ImageAdapter imageadapter;
	private static int count = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);        
		imagelist = new ArrayList<Bitmap>();		
        imagelistview = (ListView) findViewById(R.id.imagelist);
        imageadapter = new ImageAdapter(MainActivity.this,imagelist);
		imagelistview.setAdapter(imageadapter);		
		Button camera = (Button) findViewById(R.id.btncapture);
		camera.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 1);
			}
		
		});
		
		saveall = (Button) findViewById(R.id.btnsaveall);
		saveall.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				saveall();
			}
		
		})	;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {				
		
        if (requestCode == 1 && resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();
                mCameraBitmap = (Bitmap) extras.get("data");
                imagelist.add(mCameraBitmap); 
                imageadapter.insert(mCameraBitmap,count);   
                imageadapter.notifyDataSetInvalidated();
        }
	}

	private void saveall()
	{
		File saveFile;
		for (Bitmap b: imagelist)
		{		     			
			saveFile = openFileForImage(count);	
			if (saveFile != null) {
				saveImageToFile(saveFile, b);
				count++;
			} else {
				Toast.makeText(MainActivity.this, "Unable to open file for saving image.",
						Toast.LENGTH_LONG).show();
			}

		}
	}
	
	 private File openFileForImage(int i) {
		    File imageDirectory = null;
		    String storageState = Environment.getExternalStorageState();
		    if (storageState.equals(Environment.MEDIA_MOUNTED)) {
		      imageDirectory = new File(
		        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), 
		        "com.example.a2lesson8");
		      if (!imageDirectory.exists() && !imageDirectory.mkdirs()) {
		        imageDirectory = null;
		      } else {
		        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_mm_dd_hh_mm",
		          Locale.getDefault());
		        return new File(imageDirectory.getPath() +
		          File.separator + "image_" + dateFormat.format(new Date()) +i+ ".png");
		      }
		    }
		    return null;
		  }
	 
	 private void saveImageToFile(File file, Bitmap b) {
		    if (b != null) {
		      FileOutputStream outStream = null;
		      try {
		        outStream = new FileOutputStream(file);
		        if (!b.compress(Bitmap.CompressFormat.PNG, 100, outStream)) {
		          Toast.makeText(MainActivity.this, "Unable to save image to file.",
		          Toast.LENGTH_LONG).show();
		        } else {
		          Toast.makeText(MainActivity.this, "Saved image to: " + file.getPath(), Toast.LENGTH_LONG).show();
		        }
		        outStream.close();
		      } catch (Exception e) {
		        Toast.makeText(MainActivity.this, "Unable to save image to file.",
		        Toast.LENGTH_LONG).show();
		      }
		    }
		  }
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
		

}

